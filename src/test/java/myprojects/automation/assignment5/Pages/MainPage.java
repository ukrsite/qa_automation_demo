package myprojects.automation.assignment5.Pages;

import myprojects.automation.assignment5.utils.Properties;
import org.apache.commons.lang3.ObjectUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class MainPage {
    private EventFiringWebDriver driver;

    private boolean isMobileVersion = false;

    private By allProducts = By.cssSelector("a.all-product-link.pull-xs-left.pull-md-right.h4");
    private By inputProduct = By.name("s");
    private By searchButton = By.cssSelector("i.material-icons.search");
    private By mobileLogo = By.id("_mobile_cart");
    private By productTitle = By.cssSelector("a[class='thumbnail product-thumbnail']");

    private WebElement mobileLogoElement;

    public MainPage(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public void openMainPage() {
        driver.get(Properties.getBaseUrl());
    }

    public void clickAllProducts() {
        driver.findElement(allProducts).click();
    }

    public void setInputProduct(String name) {
        driver.findElement(inputProduct).sendKeys(name);
    }

    public void clickSearchButton() {
        driver.findElement(searchButton).click();
    }

    public void openProductCard() {
        driver.findElement(productTitle).click();
    }

    public Boolean isMobileSiteVersion() {
        mobileLogoElement = driver.findElement(mobileLogo);
        if (mobileLogoElement.isDisplayed()) {
            isMobileVersion = true;
        }
        return isMobileVersion;
    }
}

