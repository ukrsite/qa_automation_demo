package myprojects.automation.assignment5.Pages;

import myprojects.automation.assignment5.model.CustomerData;
import myprojects.automation.assignment5.model.ProductData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Random;

public class ProductOrder {
    EventFiringWebDriver driver =null;

    private By firstname = By.cssSelector(".form-control[name='firstname']");
    private By lastname = By.cssSelector(".form-control[name='lastname']");
    private By email = By.cssSelector(".form-control[name='email']");
    private By continu = By.cssSelector(".pull-xs-right[name='continue']");
    private By address = By.cssSelector(".form-control[name='address1']");
    private By postcode1 = By.cssSelector(".form-control[name='postcode']");
    private By city1 = By.cssSelector(".form-control[name='city']");
    private By confirm1 = By.cssSelector(".pull-xs-right[name='confirm-addresses']");
    private By confirmDeliveryOption = By.cssSelector(".pull-xs-right[name='confirmDeliveryOption']");
    private By paymentOption2 = By.id("payment-option-2");
    private By conditions_to_approve = By.id("conditions_to_approve[terms-and-conditions]");
    private By confirmPaymentOption = By.cssSelector("button.btn.btn-primary.center-block");
    private By cardTitle = By.cssSelector("h3[class='h1 card-title']");
    private By productName = By.cssSelector(".details");
    private By productQty = By.cssSelector(".col-xs-2");
    private By productPrice = By.cssSelector(".bold");


    public ProductOrder(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public String getProductPrice() {
        return driver.findElement(productPrice).getText();
    }

    public String getProductQty() {
        return driver.findElement(productQty).getText();
    }

    public String getProductName() {
        return driver.findElement(productName).getText();
    }

    public String getCardTitle() {
        return driver.findElement(cardTitle).getText();
    }

    public void setFirstname(String firstN) {
           driver.findElement(firstname).sendKeys(firstN);
    }

    public void setLastname(String lastN) {
        driver.findElement(lastname).sendKeys(lastN);
    }

    public void setEmail(String emailN) {
        driver.findElement(email).sendKeys(emailN);
    }

    public void clickContinueButton() {
        driver.findElement(continu).click();
    }

    public void setAddress(String address1) {
        driver.findElement(address).sendKeys(address1);
    }

    public void setPostcode(String postcode) {
        driver.findElement(postcode1).sendKeys(postcode);
    }

    public void setCity(String city) {
        driver.findElement(city1).sendKeys(city);
    }

    public void clickConfirmButton() {
        driver.findElement(confirm1).click();
    }

    public void clickConfirmDeliveryOptionButton() {
        driver.findElement(confirmDeliveryOption).click();
    }

    public void setPaymentOption2() {
        driver.findElement(paymentOption2).click();
    }

    public void setConditions_to_approve() {
        driver.findElement(conditions_to_approve).click();
    }

    public void clickConfirmPaymentOptionButton() {
        driver.findElement(confirmPaymentOption).click();
    }
}
