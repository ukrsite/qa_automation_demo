package myprojects.automation.assignment5.Pages;

import myprojects.automation.assignment5.utils.DataConverter;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class ProductCart {

    By name = By.cssSelector(".product-line-info > a[class='label']");
    By price = By.cssSelector(".product-line-info > span[class='value']");
    By qty = By.cssSelector("input.js-cart-line-product-quantity.form-control");

    By orderButton = By.cssSelector("a.btn.btn-primary");
    By subtotal = By.cssSelector("#cart-subtotal-products>span[class='value']");
    By total = By.cssSelector(".cart-total>span[class='value']");
    By priceBold = By.className("product-price");
    By qtySubtotal = By.className("js-subtotal");


    EventFiringWebDriver driver = null;

    public ProductCart(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public String getName() {
        return driver.findElement(name).getText();
    }

    public Float getPrice() {
        return DataConverter.parsePriceValue(driver.findElement(price).getText());
    }

    public Float getPriceBold() {
        return DataConverter.parsePriceValue(driver.findElement(priceBold).getText());
    }

    public Integer getQty() {
        return Integer.valueOf(driver.findElement(qty).getAttribute("value"));
    }

    public Integer getQtySubtotal() {
        return DataConverter.parseStockValue(driver.findElement(qtySubtotal).getText());
    }

    public Float getSubTotal() {
        return DataConverter.parsePriceValue(driver.findElement(subtotal).getText());
    }


    public Float getTotal() {
        return DataConverter.parsePriceValue(driver.findElement(total).getText());
    }

    public void clickOrderButton() {
        driver.findElement(orderButton).click();
    }
}

