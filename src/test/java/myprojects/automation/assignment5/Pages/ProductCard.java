package myprojects.automation.assignment5.Pages;

import com.sun.prism.shader.DrawSemiRoundRect_ImagePattern_AlphaTest_Loader;
import myprojects.automation.assignment5.utils.DataConverter;
import org.openqa.selenium.By;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class ProductCard {

    private EventFiringWebDriver driver;

    private String qtyTitle;
    private String qtyAll;

    private By productDetails = By.cssSelector("a[href='#product-details']");
    private By goOrderButton = By.cssSelector("a.btn.btn-primary");
    private By addToCart = By.className("add-to-cart");
    private By productName = By.cssSelector("h1[itemprop='name']");
    private By productPrice = By.cssSelector("span[content]");
    private By productQtyAll = By.cssSelector("div.product-quantities > span");
    private By getProductQtyWanted = By.id ("quantity_wanted");

    public ProductCard(EventFiringWebDriver driver) {
        this.driver = driver;
    }

    public String getProductName() {
        return driver.findElement(productName).getText();
    }

    public Float getProductPrice() {
        return DataConverter.parsePriceValue(driver.findElement(productPrice).getText());
//        String s = driver.findElement(productPrice).getText();
//        String ss = s.split("\\ ")[0];
//        return DataConverter.parsePriceValue (ss);
    }

    public Integer getQtyWanted() {
        qtyAll = driver.findElement(getProductQtyWanted).getAttribute("value");
        return Integer.valueOf(qtyAll);
    }

    public Integer getProductQtyAll() {
        qtyTitle = driver.findElement(productQtyAll).getText();
        return DataConverter.parseStockValue(qtyTitle);
    }

    public void addProductToCart() {
        driver.findElement(addToCart).click();
    }

    public void goToOrder() {
        driver.findElement(goOrderButton).click();
    }

    public void clickProductDetails() {
        driver.findElement(productDetails).click();
    }
}