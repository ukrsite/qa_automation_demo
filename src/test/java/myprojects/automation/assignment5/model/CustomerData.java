package myprojects.automation.assignment5.model;

import java.util.Random;

/**
 * Hold Customer information that is used among tests.
 */
public class CustomerData {
    String strFirstname;
    String strLastname;
    String strEmail;
    String address1;
    String postcode;
    String city;


    public CustomerData(String firstname, String  lastname, String  email,
                        String address1, String postcode, String city) {
        this.strFirstname = firstname;
        this.strLastname = lastname;
        this.strEmail = email;
        this.address1 = address1;
        this.postcode = postcode;
        this.city = city;
    }

    public String getFirstname() {
        return strFirstname;
    }

    public String getLastname () {
        return strLastname;
    }

    public String getEmail() {
        return strEmail;
    }

    public String getAddress1() {
        return address1;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getCity() {
        return city;
    }

    /**
     * @return New Customer object with random firstname, lastname and email values.
     */
    public static CustomerData generate() {
        Random random = new Random();
        return new CustomerData(
                "New",
                "Customer",
                "New" + System.currentTimeMillis() + "@ukr.net",
                "Azovskij Ave.",
                "73112",
                "Berdyansk");
    }
}
