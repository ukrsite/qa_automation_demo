package myprojects.automation.assignment5.tests;

import myprojects.automation.assignment5.BaseTest;
import myprojects.automation.assignment5.model.CustomerData;
import myprojects.automation.assignment5.model.ProductData;
import org.testng.annotations.Test;

public class PlaceOrderTest extends BaseTest {

    @Test
    public void checkSiteVersion() {
        // TODO open main page and validate website version
        actions.checkSiteVersion(isMobileTesting);
    }

    @Test (dependsOnMethods = "checkSiteVersion")
    public void createNewOrder() {
        // TODO implement order creation test

        ProductData productData = null;
        CustomerData customerData = null;

        // open random product
        actions.openRandomProduct();
        // save product parameters
        productData = actions.getOpenedProductInfo();
        // add product to Cart and validate product information in the Cart
        actions.addProductToCart();
        actions.validateProductInfo(productData);
        // proceed to order creation, fill required information
        customerData = CustomerData.generate();
        actions.fillCustomerData(customerData);
        // place new order and validate order summary
        actions.validateOrderSummary(productData);
        // check updated In Stock value
        actions.checkUpdateInStockValue(productData);
    }

}
